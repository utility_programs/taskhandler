/**
 * @file TaskHandler.cpp
 * @author Julian Neundorf
 * @brief
 * @version 0.1
 * @date 2023-12-21
 *
 * Remarks:
*/
#include <TaskHandler.hpp>

#include <cstdint>

#include "Arduino.h"
#include "WIFIClient.hpp"

extern std::uint32_t getMillis();

void TaskHandler::add(Task* task) {
  tasks_.push_back(task);
}

void TaskHandler::run() {
  for (Task* t : tasks_)
    t->run();
}


Task::Task(std::uint32_t _delayMillis, bool _active) : active_(_active), delayMillis_(_delayMillis) {
  TaskHandler::getInstance().add(this);
}

void Task::run() {
  if (this->active_ && (getMillis() - this->lastRun_ > this->delayMillis_)) {
    this->lastRun_ = getMillis();
    this->run_();
  }
}

void Task::start() {
  active_ = true;
}

void Task::stop() {
  active_ = false;
}