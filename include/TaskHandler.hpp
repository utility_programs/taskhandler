/**
 * @file TaskHandler.hpp
 * @author Julian Neundorf
 * @brief
 * @version 0.1
 * @date 2023-12-21
 *
 * Remarks:
*/
#pragma once

#include <cstdint>
#include <vector>

class Task {
public:
  Task(std::uint32_t _delayMillis = 1, bool _active = true);

  void run();

  void start();
  void stop();

  bool isActive() { return active_; }

  void setDelayMillis(std::uint32_t _delayMillis) { delayMillis_ = _delayMillis; }

private:
  bool active_ = false;
  std::uint32_t delayMillis_ = 0;
  std::uint32_t lastRun_ = 0;

  virtual void run_() = 0;
};

class TaskHandler {
public:
  static TaskHandler& getInstance() {
    static TaskHandler taskHandler;
    return taskHandler;
  }

  void add(Task* task);
  void run();

private:
  TaskHandler() = default;

  std::vector<Task*> tasks_;
};
